﻿open System
open System.IO
open System.Threading

let historyTextFile = "C:\Users\kanep_000\Desktop\Task3_Resubmission\Calculator_Task3\History.txt"
let sumsTextFile = @"C:\Users\kanep_000\Desktop\Task3_Resubmission\Calculator_Task3\Sums.txt"

//entering a number to calculator
let enterNum() = 
    Console.Write("Please enter a number : ")
    let input = Console.ReadLine()
    input

//writing to a file for history
let writeToFile fileName sum = 
   File.AppendAllText(historyTextFile,sum + "\n")

//open history
let sumHistory fileName =
    let sums = File.ReadAllLines(fileName)
    for s in sums do
        printfn("%s") s


let opToFun = function
    | "+" -> ( + )
    | "-" -> ( - )
    | "*" -> ( * )
    | "/" -> ( / )
    | _ -> failwith "Unknown operator"

let calc op x y =
    let f = opToFun op
    f (int x) (int y)


let Sum f = 
    let x = enterNum() 
    let y = enterNum()
    let z = calc f x y
    let sum = sprintf "%s %s %s = %i" x f y z
    printfn"%s" sum
    writeToFile sumsTextFile sum
    

let readFromConsole sumString = 
    let input = Console.ReadLine()
    input



//function for writing a sum as a string and calculating 
let sumOfString(input: string) = 
    let wholeLine : string[] = input.Split[|' '|]
    let num1 : int = int32 wholeLine.[0]
    let operator : string = wholeLine.[1]
    let num2 : int = int32 wholeLine.[2]
    let result = calc wholeLine.[1] wholeLine.[0] wholeLine.[2]
    result
    


// function for reading in "sums.txt" file
let readFromFile(fileName : string) = 
    let sumsFile = File.ReadAllLines(fileName)
    printfn "Successfully Read in the following Sums: \n" 
    let result = Array.Parallel.map sumOfString sumsFile
    Array.iter2(printf"%s = %i\n") sumsFile result



//used for menu selection in Calc
let menuSelection input = 
    match input with 
    | "1" -> Sum"+"
    | "2" -> Sum "-"
    | "3" -> Sum "*"
    | "4" -> Sum "/"
    | "5" -> let input = readFromConsole()
             let output = sumOfString input
             printfn"%s = %i" input output
    | "6" -> sumHistory(historyTextFile)
    | "7" -> readFromFile(sumsTextFile)
    | "8" -> exit(0)
    | _ -> printfn("Please Select a valid option")

[<EntryPoint>]

let main args = 
    let mutable calcRunning = true
    while calcRunning do
         Console.Write("Choose an operation: \n1. Addition \n2. Subtraction \n3. Multiplication \n4. Division\n5: Enter Sum As a string e.g 1 + 1 (with spaces)\n6: show sum history\n7: Load from File\n8: Exit \n\n")
         let selection = Console.ReadLine()
         menuSelection selection
         
         Console.WriteLine("Use calculator again? Y/N")
         let useAgain = Console.ReadLine()
       
         match useAgain with 
         | "Y" | "y" -> Console.Clear()
         | "N" | "n" ->  calcRunning <- false
         | _ -> printfn "Please Enter y/n or Y/N "

 
    0 
